package assignment5;

import java.io.*;
import java.util.*;


public class Dictionary {

	private HashSet<String> dataBase = new HashSet<>();
	private String fileName = "assn5words.dat";
	
	/**
	 * Generates the dictionary database based on a given input file of words
	 */
	Dictionary()
	{
		try{
			Scanner scanner = new Scanner(new File(fileName));
			String line = scanner.nextLine();
			while (scanner.hasNext())
			{
				line = scanner.nextLine();
				if (line.charAt(0) != '*')
				{
					dataBase.add(line.substring(0, 5));
				}
			}
			scanner.close();
		}
		catch(FileNotFoundException e){
			System.out.println("Dictionary file not found");
		} 
		catch(Exception e)
		{
			
		}
	}

	/**
	 * Searches the dictionary to see if it contains the provided word
	 * @param newWord the word to be searched for
	 * @return true if the word is found, false if the word is not in the dictionary
	 */
	public boolean containsWord(String newWord) 
	{
		return dataBase.contains(newWord);
	}
}