package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.lang.IllegalArgumentException;
public class A5Driver
{
	private static final File filename = new File("wordPairs.txt"); //This is the file name where the program get inputs
	public static void main(String[] args)
	{
		// Create a word ladder solver object
		Assignment5Interface wordLadderSolver = new WordLadderSolver();
		StopWatch sw = new StopWatch();
		
		// Read file in
		// for each line in the file call compute ladder
		try
		{
			Scanner scanner = new Scanner(filename);
			while (scanner.hasNext())
			{
				try 
				{
					String firstWord = scanner.next();
					String secondWord = scanner.next();
					System.out.println("For the input words \"" + firstWord + "\" and \"" + secondWord + "\"");
					sw.start();
					List<String> result = wordLadderSolver.computeLadder(firstWord, secondWord);
					sw.stop();
					// boolean isCorrectWordLadder = wordLadderSolver.validateResult(firstWord, secondWord, result);
					for (String str : result)
					{
						System.out.println(str);
					}
					
					System.out.println("Total time to compute ladder: " + sw.getElapsedTime() / StopWatch.NANOS_PER_SEC + " seconds");
					sw.reset();
				} 
				catch (NoSuchLadderException e) 
				{

				}
				catch (IllegalArgumentException e)
				{
					System.out.println(e.getMessage());
				}
				System.out.println("***********");

			}
			scanner.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Sorry but it didn't find your file. Make sure you got the file name right.");
			System.out.println("To change the file name, change the static class variable filename.");
			System.out.println("Stopping the program. Try again. And get the spelling right this time dang it!");
		}
	}
}
