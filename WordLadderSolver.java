/*
 * Nhan Do (ntd342)
 * Joey Thometz (jat3829)
 * Negin Mohamadi
 * EE422C-Assignment 5
 */

package assignment5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// do not change class name or interface it implements
public class WordLadderSolver implements Assignment5Interface
{
	// declare class members here.
	Dictionary dictionary;
	ArrayList<String> solutionList;
	ArrayList<String> seenWords;
	String alphabet = "abcdefghijklmnopqrstuvwxyz";
	
	/**
	 * 
	 */
	WordLadderSolver()
	{
		dictionary = new Dictionary();
	}

	// do not change signature of the method implemented from the interface
	@Override
	public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException 
	{
		solutionList = new ArrayList<>();
		seenWords = new ArrayList<>();
		
		if (dictionary.containsWord(startWord) && dictionary.containsWord(endWord)){
			if (makeLadder(startWord, endWord, -1)){
				return solutionList;
			} else {
				throw new NoSuchLadderException("There is no word ladder between " + startWord + " and " + endWord + "!");
			}
		}
		else
		{
			throw new IllegalArgumentException("At least one of the words " + startWord + " and " + endWord + " are not legitimate 5-letter, lower case words from the dictionary");
		}
		
		
	}

	@Override
	//Validates that a word ladder is a real word ladder
	public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
	{
		seenWords = new ArrayList<>();
		String firstWord = wordLadder.get(0);
		String secondWord = wordLadder.get(wordLadder.size()-1);
		seenWords.add(firstWord);
		
		//	check if the first and last words of the word ladder provided are the same as the start and end words provided
    	if(!(startWord.equals(firstWord) && endWord.equals(secondWord)))
    	{
    		return false;
    	}
		
    	//	iterate through each word in the ladder, checking if it differs from the word before it by exactly 1 character
    	for (int i = 0; i < wordLadder.size() - 1; i++) 
    	{
    		// get the next word in the word ladder
    		secondWord = wordLadder.get(i + 1);
    		
    		// check if word has already been used in the ladder (all words must be distinct)
    		if (seenWords.contains(secondWord))
    		{
    			return false;
    		}
    		seenWords.add(secondWord);
    		
    		//find the number of different characters between the words
    		int difference = getNumberOfDiffChars(secondWord, firstWord); //keeps track of differences between each letter
    		
    		// if the difference is not equal to 1, then the word ladder is invalid
    		if (difference != 1)
    		{ 
    			return false; 
    		}
    		firstWord = secondWord;
    	}
    	
    	// if every word in the ladder passes the above validation tests, then the word ladder is valid
        return true;
	}

	/**
	 * Recursively finds a word ladder
	 * @param fromWord the starting word
	 * @param toWord the final word in the word ladder to be generated
	 * @param changedCharPos the position of the character changed from the previous word in the ladder
	 * @return true if a word ladder would be generated, false if no solutions exist
	 */
	boolean makeLadder(String fromWord, String toWord, int changedCharPos)
	{
		ArrayList<String> candidateList = new ArrayList<>();
		
		if(seenWords.contains(fromWord)){
			return false;
		}
		
		solutionList.add(fromWord);
		seenWords.add(fromWord);
		
		// PART I: Create the list of candidate words
		// iterate through every character position in the fromWord
		for (int p = 0; p < fromWord.length(); p++)
		{
			// check if the current position is the same as the position just changed
			if (p != changedCharPos)
			{
				// iterate through every letter in the alphabet
				for (int c = 0; c < alphabet.length(); c++)
				{
					
					// check if the replacement letter is the same as the old one
					if (alphabet.charAt(c) != fromWord.charAt(p))
					{
						StringBuilder newWord = new StringBuilder(fromWord);
						newWord.setCharAt(p, alphabet.charAt(c));
						
						// check if the new String created is a valid word in the dictionary, and is not already in the solution list
						if (dictionary.containsWord(newWord.toString()) && !solutionList.contains(newWord.toString()))
						{
							int diff = getNumberOfDiffChars(newWord.toString(), toWord);
							
							// if the word differs by only one letter then a solution has been found
							if (diff == 1)
							{
								solutionList.add(newWord.toString());
								solutionList.add(toWord);
								return true;
							}
							
							// add the number of different letters to the front, 
							// and the position of the letter changed to the back (to be used in PART II)
							newWord.insert(0, diff);
							newWord.append(p);
							
							candidateList.add(newWord.toString());
						}
					}
				}
			}
		}
		
		//PART II: Search the candidate list of words for a solution
		Collections.sort(candidateList);
		
		for (String candidateWord : candidateList)
		{
			int candidateChangedPos = Integer.parseInt(candidateWord.substring(candidateWord.length() - 1));
			String nextWord =  candidateWord.substring(1, candidateWord.length() - 1);
			
			if (makeLadder(nextWord, toWord, candidateChangedPos))
			{
				return true;
			}
		}
		
		solutionList.remove(fromWord);
		return false;
	}
	
	/**
	 * Finds the number of character positions between two Strings with differing characters
	 * @param word1 first word to be compared
	 * @param word2 second word to be compared
	 * @return the total number of differing character positions
	 */
	int getNumberOfDiffChars(String word1, String word2)
	{
		// find the number of differing characters
		int diff = 0;
		for (int i = 0; i < word1.length(); i++)
		{
			if (word2.charAt(i) != word1.charAt(i))
			{
				diff++;
			}
		}
		
		return diff;
	}
	
}
